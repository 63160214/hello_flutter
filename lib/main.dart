// import 'package:flutter/material.dart';
// void main() => runApp(HelloFlutterApp());
// //safeArea Widget
// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context){
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//      home: Scaffold(
//        appBar: AppBar(
//          title: Text("Hello Flutter"),
//          leading: Icon(Icons.home),
//          actions: <Widget>[
//            IconButton(
//                onPressed: () {},
//                icon: Icon(Icons.refresh)
//            )
//          ],
//        ) ,
//        body: Center(
//          child: Text(
//            "Hello Flutter",
//            style: TextStyle(fontSize: 24),
//          ),
//        ),
//      ),
//     );
//   }
// }

import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());
class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Halo Flutter";
String ford = "ford ranger raptor";
String thai = "สวัสดี Flutter";

class _MyStatefulWidgetState extends State <HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
     home: Scaffold(
       appBar: AppBar(
         title: Text("Hello Flutter"),
         leading: Icon(Icons.home),
         actions: <Widget>[
           IconButton(
               onPressed: () {
                 setState(() {
                   displayText = displayText == englishGreeting?
                   spanishGreeting: englishGreeting;
                 });
               },
               icon: Icon(Icons.refresh)
           ),
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?
                  ford: englishGreeting;
                });
              },
            ),
           IconButton(
               onPressed: () {
                 setState(() {
                   displayText = displayText == englishGreeting?
                   thai: englishGreeting;
                 });
               },
               icon: Icon(Icons.airplay)
           ),
      ]
       ) ,
       body: Center(
         child: Text(
           displayText,
           style: TextStyle(fontSize: 24),
         ),
       ),
     ),
    );
    return Container();
  }
}